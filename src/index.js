import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './containers/App'
import registerServiceWorker from './registerServiceWorker'
import { Provider } from 'react-redux'
import createStore from './store'
import 'semantic-ui-css/semantic.min.css'
import './app.css';

const store = createStore()

const app =(
  <Provider store={store}>
      <App />
  </Provider>
)


ReactDOM.render(app, document.getElementById('root'))
registerServiceWorker()
